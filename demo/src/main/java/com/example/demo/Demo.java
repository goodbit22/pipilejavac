package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class Demo {
    @RequestMapping("/test")
    @ResponseBody
    public String test1(){
        return "to jest przykladowy EndPoint Rest";
    }
    public Integer mul(Integer x, Integer y){
        return x * y;
    }
}
